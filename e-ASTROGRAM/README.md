# e-ASTROGRAM : Smoking gun dark matter signatures in the MeV range 

This is the code used to run the Pythia simulation behind and create the plot
to the right in Figure 4.4.1 in _Science with e-ASTROGAM: A space mission for 
MeV-GeV gamma-ray astrophysics_, published in the [Journal of High Energy
Astrophysics](https://doi.org/10.1016/j.jheap.2018.07.001), available also on
the [arXiv](https://arxiv.org/abs/1711.01265).


## Dependencies

 - Pythia8 (http://home.thep.lu.se/Pythia/pythia82php/Welcome.php) 

 - pyyaml (https://pyyaml.org/) (pip install pyyaml --user)
 
  - - - -

New HEPP-ers can of course feel free to use this code as an introduction to

doing a Pythia run, storing energy information and plotting, and any questions

concerning the code can be directed to inga (at) strumke (dot) com.
