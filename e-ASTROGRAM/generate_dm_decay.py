"""
Initialize event with particle Upsilon(10860), PDG code 9000553 and hardcoded
decay table, back-to-back with a photon.
The energy in the event is determined by the total mass of the two Dark Matter
particles.

The Upsilon is decayed by Pythia, and the energies of the final photons from
different mesons are stored in numpy arrays, saved to numpy files.

Usage:
python generate_dm_decay.py

Optional arguments:
-dir (Output directory. By default run_1)
-n (Number of events. By default 1 million)
-paramfile (Pythia parameter file. By default the provided cmnd.cmnd)
-m (Dark Matter mass in GeV. By default 5.5 GeV)
"""

import os
import math
import random
from argparse import ArgumentParser
import sys
import numpy as np
import yaml


# ---------------------------------------------------------------------------
# --- Import Pythia

try:
    import pythia8

except ImportError:

    # --- In case the pythia8 import does not work, a workaround is to
    # --- manually specify the path to pythiaxxx/lib:

    #PYTHIAPATH = "/home/your/own/path/to/pythiaxxxx/lib"
    #sys.path.insert(0, PYTHIAPATH)
    #import pythia8

    # --- If this is done, uncomment the above and comment out below
    print "Please provide path to Pythia. Standard import didn't work."
    sys.exit()

PYTHIA = pythia8.Pythia()

# ---------------------------------------------------------------------------
# --- Functions

def get_mother_ids(event_index):
    """
    From pythia.event and pythia Particle object, return a list of the
    particle's mother's id(s)
    """

    mother_list = PYTHIA.event[event_index].motherList()

    return list(PYTHIA.event[particle_i].id() for particle_i in mother_list)

def breit_wigner(mean, gamma):
    """
    Returns a randomly drawn sample from the Breit-Wigner distribution.

    Used to model the Upsilon as a resonance using the resonnance mass as mean
    value, and the resonance width as gamma.
    """

    random_value = 2*random.random()-1
    displacement = 0.5*gamma*math.tan(random_value*math.pi/2)

    return mean+displacement

# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# --- Collect arguments

PARSER = ArgumentParser(description="Decay Upsilon(10860) with "\
        "a back-to-back photon, representing annihilation of "\
        "two Dark Matter particles.")

PARSER.add_argument("-dir", "--directory", default="run_1",
                    help="Output directory. Default: run_1")

PARSER.add_argument("-n", "--events", default=1000000,
                    help="Number of events. Default: 1 million")

PARSER.add_argument("-paramfile", "--paramfile", default="cmnd.cmnd",
                    help="Pythia parameter file. Default: cmnd.cmnd")

PARSER.add_argument("-m", "--mass", default=5.5,
                    help="Dark matter mass in GeV. Default: 5.5 GeV")

PARGS = PARSER.parse_args()

DIR = PARGS.directory

if not os.path.exists(DIR):
    os.makedirs(DIR)

N_EVENTS = int(PARGS.events)

# ---------------------------------------------------------------------------
# --- Dark Matter and Upsilon parameters

MASS_DM = float(PARGS.mass)

P_X = P_Y = 0

STATUS = 1
COLOUR_TYPE = 0
SPIN_TYPE = 3


MASS_UPSILON = 10.8911  # Upsilon mass in GeV
MASS_MIN = 10.0         # GeV
MASS_MAX = 12.0         # GeV
GAMMA_UPSILON = 0.0537  # Upsilon total width in  GeV

# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# ---  Initialize Pythia

PDG_CODE = 9000553

PYTHIA.readFile(PARGS.paramfile)

PYTHIA.readString("{0}:new".format(PDG_CODE))
PYTHIA.readString("{0}:name = Upsilon(10860)".format(PDG_CODE))
PYTHIA.readString("{0}:spinType = {1}".format(PDG_CODE, SPIN_TYPE))
PYTHIA.readString("{0}:chargeType = 0".format(PDG_CODE))
PYTHIA.readString("{0}:colType = {1}".format(PDG_CODE, COLOUR_TYPE))
PYTHIA.readString("{0}:m0 = {1}".format(PDG_CODE, MASS_UPSILON))
PYTHIA.readString("{0}:mMin = {1}".format(PDG_CODE, MASS_MIN))
PYTHIA.readString("{0}:mMax = {1}".format(PDG_CODE, MASS_MAX))
PYTHIA.readString("{0}:mWidth = {1}".format(PDG_CODE, GAMMA_UPSILON))


PYTHIA.readString("{0}:addChannel = 1 0.0638 0 511 -511".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0795 0 511 -513".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0795 0 -511 513".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.4420 0 513 -513".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0423 0 513 -511 111".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0423 0 -513 511 111".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0116 0 513 -513 111".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.0052 0 531 -531".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.007 0 531 -533".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.007 0 -531 533".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.1813 0 533 -533".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.01123 0 553 211 -211".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.01654 0 100553 211 -211".format(PDG_CODE))
PYTHIA.readString("{0}:addChannel = 1 0.01018 0 200553 211 -211".format(PDG_CODE))

PYTHIA.init()

# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# --- Arrays and parameters for event loop

E_FINAL_PHOTONS = []            # All final photons
E_UPSILON_SISTER_PHOTONS = []   # The photon decaying with the Upsilon

E_533_PHOTONS = []              # Photons from B*_s decay
E_513_PHOTONS = []              # Photons from B_s decay
E_111_PHOTONS = []              # Photons from pion decay

OTHER_PHOTONS = []              # Photons from other decays

COLOUR, ANTICOLOUR = 0, 0

BSCOUNT = 0
REJECTED = 0
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
for ev in range(N_EVENTS):

    # --- Get Upsilon mass from Breit Wigner distribution
    random.seed()
    m_Upsilon = breit_wigner(MASS_UPSILON, GAMMA_UPSILON)

    # --- Check that Upsilon mass is kinematically and physically possible
    while (m_Upsilon > 2*MASS_DM or
           m_Upsilon <= 0 or
           abs(m_Upsilon-MASS_UPSILON) > 10*GAMMA_UPSILON):

        m_Upsilon = breit_wigner(MASS_UPSILON, GAMMA_UPSILON)
        REJECTED += 1

    # --- Generate Upsilon and gamma back to back
    _P_Z = (4*MASS_DM**2-m_Upsilon**2)/(4*MASS_DM)
    E_Upsilon = math.sqrt(m_Upsilon**2 + _P_Z**2)

    # --- Reset and generate new event, filled with one Upsilon and an opposite photon
    Bsds = []
    Bs = False

    PYTHIA.event.reset()

    # --- Add Upsilon to event
    PYTHIA.event.append(PDG_CODE,
                        STATUS,
                        COLOUR, ANTICOLOUR,
                        P_X, P_Y, _P_Z,
                        E_Upsilon,
                        m_Upsilon)

    # --- Add photon back-to-back with Upsilon
    PYTHIA.event.append(22,
                        STATUS,
                        COLOUR, ANTICOLOUR,
                        P_X, P_Y, -_P_Z,
                        abs(_P_Z), 0)

    # --- Sanity checks ------------------------------------------------------------

    if _P_Z < 0:
        print "Discarding event due do negative photon energy."
        PYTHIA.event.list()
        continue

    if _P_Z > 4:
        print "Discarding event due to too large upsilon / gamma momentum."
        PYTHIA.event.list()
        continue

    if not PYTHIA.next():
        print "ERROR. Continue to next event."
        continue

    # --- Event -------------------------------------------------------------------

    E_UPSILON_SISTER_PHOTONS.append(abs(_P_Z))

    for particle_no in range(PYTHIA.event.size()):

        if PYTHIA.event[particle_no].id() == 22 and PYTHIA.event[particle_no].isFinal:
            E_FINAL_PHOTONS.append(PYTHIA.event[particle_no].e())

            if 533 in get_mother_ids(particle_no):
                E_533_PHOTONS.append(PYTHIA.event[particle_no].e())

                BSCOUNT += 1

            elif -533 in get_mother_ids(particle_no):
                E_533_PHOTONS.append(PYTHIA.event[particle_no].e())

            elif 513 in get_mother_ids(particle_no):
                E_513_PHOTONS.append(PYTHIA.event[particle_no].e())

            elif -513 in get_mother_ids(particle_no):
                E_513_PHOTONS.append(PYTHIA.event[particle_no].e())

            elif 111 in get_mother_ids(particle_no):
                E_111_PHOTONS.append(PYTHIA.event[particle_no].e())

            else:
                OTHER_PHOTONS.append(PYTHIA.event[particle_no].e())

# ---------------------------------------------------------------------------

with open(os.path.join(DIR, "_for_plot.yaml"), 'w') as _yaml_file:
    plotinfo = {"nevents" : N_EVENTS, "mass" : MASS_DM}
    yaml.dump(plotinfo, _yaml_file)

# ---------------------------------------------------------------------------
# --- Print information and save energy arrays

print "Events with B*_s: ", 1.0*BSCOUNT/(1.0*N_EVENTS)*100, ' %'
print "Events rejected due to unphysical mass: ", REJECTED/N_EVENTS*100, ' %'

np.save(os.path.join(DIR, "gamma_DM_annihilation"),
        np.array(E_FINAL_PHOTONS))
print "Total number of photons: {0}".format(len(E_FINAL_PHOTONS))

np.save(os.path.join(DIR, "gamma_back_upsilon"),
        np.array(E_UPSILON_SISTER_PHOTONS))
print "Photons back to Upsilon: {0}".format(len(E_UPSILON_SISTER_PHOTONS))

np.save(os.path.join(DIR, "gamma_533"),
        np.array(E_533_PHOTONS))
print "Photons from B star: {0}".format(len(E_533_PHOTONS))

np.save(os.path.join(DIR, "gamma_513"),
        np.array(E_513_PHOTONS))
print "Final photons from 513: {0}".format(len(E_513_PHOTONS))

np.save(os.path.join(DIR, "gamma_111"),
        np.array(E_111_PHOTONS))
print "Final photons from 111: {0}".format(len(E_111_PHOTONS))

# --- Uncomment this to record photons from other decays
#np.save(os.path.join(DIR, "gamma_other"),
#        np.array(OTHER_PHOTONS))
print "Final photons from all other decays: {0}".format(len(OTHER_PHOTONS))

# ---------------------------------------------------------------------------
